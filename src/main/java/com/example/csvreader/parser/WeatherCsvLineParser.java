package com.example.csvreader.parser;

import com.example.csvreader.exception.ParserException;
import com.example.csvreader.model.WeatherLine;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class WeatherCsvLineParser {

  private static final DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

  private static final int NUM_OF_COLUMNS = 3;
  private static final int CITY_COLUMN = 0;
  private static final int DATE_COLUMN = 1;
  private static final int TEMP_COLUMN = 2;

  public static WeatherLine parseLine(String[] args) throws ParserException {
    if (args.length < NUM_OF_COLUMNS) {
      throw new ParserException("Exception while parsing line: " + Arrays.toString(args));
    } else {
      final String city = args[CITY_COLUMN].trim();
      final Integer year = getYear(args[DATE_COLUMN].trim());
      final Float temp = Float.valueOf(args[TEMP_COLUMN]);

      return WeatherLine.builder()
          .city(city)
          .year(year)
          .temperature(temp)
          .build();
    }
  }

  private static Integer getYear(String date) throws ParserException {
    try {
      return LocalDateTime.parse(date, formatter).getYear();
    } catch (Exception e) {
      throw new ParserException("Exception while parsing date: " + date);
    }
  }
}
