package com.example.csvreader.controller;

import com.example.csvreader.model.WeatherElemResponse;
import com.example.csvreader.service.WeatherService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
public class WeatherController {

  private final WeatherService weatherService;

  WeatherController(WeatherService weatherService) {
    this.weatherService = weatherService;
  }

  @GetMapping("/average-temperature/{city}")
  public List<WeatherElemResponse> getAverageTemperature(@PathVariable String city) {
    return weatherService.getAverageWeatherPerYear(city);
  }
}
