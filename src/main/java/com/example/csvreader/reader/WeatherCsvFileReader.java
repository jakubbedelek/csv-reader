package com.example.csvreader.reader;

import com.example.csvreader.parser.WeatherCsvLineParser;
import com.example.csvreader.model.WeatherElem;
import com.example.csvreader.model.WeatherLine;
import com.example.csvreader.model.WeatherState;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import org.yaml.snakeyaml.parser.ParserException;

public class WeatherCsvFileReader {

  private static final char SEPARATOR = ';';

  public static Collection<WeatherElem> readFile(String path, String city) {
    try (CSVReader reader = new CSVReaderBuilder(new FileReader(path))
        .withCSVParser(new com.opencsv.CSVParserBuilder().withSeparator(SEPARATOR).build())
        .build()) {

      String[] line;
      WeatherState weatherState = new WeatherState();
      while ((line = reader.readNext()) != null) {
        final WeatherLine weatherLine = mapLine(line);

        if (weatherLine != null && weatherLine.getCity().equals(city)) {
          weatherState.updateWeather(weatherLine);
        }
      }

      return weatherState.getState();
    } catch (IOException | CsvValidationException e) {
      e.printStackTrace();
      return null;
    }
  }

  private static WeatherLine mapLine(String[] args) {
    try {
      return WeatherCsvLineParser.parseLine(args);
    } catch (ParserException e) {
      System.err.println("Unable to parse line with args: " + Arrays.toString(args));
      return null;
    }
  }

}
