package com.example.csvreader.service;

import com.example.csvreader.reader.WeatherCsvFileReader;
import com.example.csvreader.model.WeatherElemResponse;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class WeatherServiceImpl implements WeatherService {

  private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");

  @Value("${weather.csvfile.path}")
  private String csvFilePath;

  public List<WeatherElemResponse> getAverageWeatherPerYear(String city) {
    final var result = WeatherCsvFileReader.readFile(csvFilePath, city);
    return Optional.ofNullable(result).map(
        elems -> elems.stream().map(
            el -> WeatherElemResponse.builder()
                .year(String.valueOf(el.getYear()))
                .averageTemperature(
                    Float.valueOf(decimalFormat.format(el.getAverageTemperature()).replace(",", "."))
                ).build()
        ).collect(Collectors.toList())
    ).orElse(null);
  }

}
