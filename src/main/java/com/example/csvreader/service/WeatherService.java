package com.example.csvreader.service;

import com.example.csvreader.model.WeatherElemResponse;
import java.util.List;

public interface WeatherService {
  List<WeatherElemResponse> getAverageWeatherPerYear(String city);
}
