package com.example.csvreader.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class WeatherLine {
  final String city;
  final Integer year;
  final Float temperature;
}
