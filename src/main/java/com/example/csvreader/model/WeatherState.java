package com.example.csvreader.model;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class WeatherState {
  private final Map<Integer, WeatherElem> weather = new TreeMap<>();

  public void updateWeather(WeatherLine weatherLine) {
    final WeatherElem weatherElem = weather.get(weatherLine.getYear());

    if (weatherElem != null) {
      weatherElem.addTemperature(weatherLine);
    } else {
      weather.put(weatherLine.getYear(), WeatherElem.createWeatherElem(weatherLine));
    }
  }

  public Collection<WeatherElem> getState() {
    return weather.values();
  }
}
