package com.example.csvreader.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class WeatherElemResponse {
  private final String year;
  private final Float averageTemperature;
}
