package com.example.csvreader.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class WeatherElem {
  private Float tempSum;
  private Integer counter;
  private Integer year;

  public void addTemperature(WeatherLine weatherLine) {
    this.tempSum += weatherLine.getTemperature();
    this.counter += 1;
  }

  public Float getAverageTemperature() {
    return tempSum / counter.floatValue();
  }

  public static WeatherElem createWeatherElem(WeatherLine weatherLine) {
    return WeatherElem.builder()
        .tempSum(weatherLine.getTemperature())
        .counter(1)
        .year(weatherLine.getYear())
        .build();
  }
}
